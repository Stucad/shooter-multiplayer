using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.VFX;

public class Gun_Controlles : MonoBehaviour
{
    #region Variable
    public Player_Controller player;
    public Gun[] guns;
    public Gun actualGun;
    public int indexGun = 0;
    public int maxGuns = 3;
    

    public GameObject prefBullHole;
    float lastShootTime = 0;
    public float reloadTime;
    public bool timeReload;
    Vector3 currentRotation;
    Vector3 targetRotation;
    public float returnSpeed;
    public float snappines;


    public int actualArmon;
    float changeTimeM;
    float lastChangeTime;
    bool isChangin;

    public float distance;
    public LayerMask lm;

    public VisualEffect vfx;

    public AudioClip recarga;
    public AudioClip changeGun;
    #endregion

    #region Unity Funtions
    private void Update()
    {
        if(Input.GetButtonDown("E") && Physics.Raycast(player.cam.transform.position, player.cam.transform.forward, out RaycastHit hit,distance,lm))
        {
            Transform gun = hit.transform;
            if(hit.transform !=  null)
            {
                Interaccion(gun, indexGun);
            }
        }
        if (actualGun != null)
        {
            if (lastShootTime <= 0 && !timeReload)
            {

                if (!actualGun.data.automatic)
                {

                    if (Input.GetButtonDown("Fire1"))
                    {
                        if (actualGun.data.actualAmmo > 0)
                        {
                            Shoot();
                        }
                    }

                }
                else
                {
                    if (Input.GetButton("Fire1"))
                    {

                        if (actualGun.data.actualAmmo > 0)
                        {
                            Shoot();
                        }
                    }
                }
            }

            if (Input.GetButtonDown("Reload") && !timeReload)
            {

                if (actualGun.data.actualAmmo < actualGun.data.maxAmmoCount)
                {
                    reloadTime = 0;
                    AudioSource.PlayClipAtPoint(recarga, gameObject.transform.position);
                    timeReload = true;
                }
            }

        }

        else
        {

        }

        if (lastShootTime >= 0)
        {
            lastShootTime -= Time.deltaTime;
        }

        if (timeReload)
        {
            reloadTime += Time.deltaTime;

            if (reloadTime >= actualGun.data.reloadTime)
            {
                timeReload = false;
                Reload_Time();
            }

        }

        targetRotation = Vector3.Lerp(targetRotation, Vector3.zero, returnSpeed * Time.deltaTime);
        currentRotation = Vector3.Slerp(currentRotation, targetRotation, snappines * Time.deltaTime);
        player.recoil.localRotation = Quaternion.Euler(currentRotation);

        if (Input.GetButtonDown("Gun1") && !timeReload)
        {
            if (indexGun != 0)
            {
                indexGun = 0;
                lastChangeTime = 0;
                if (actualGun != null)
                {
                    actualGun.gameObject.SetActive(false);
                    actualGun = null;
                }
                isChangin = true;
            }
        }


        if (Input.GetButtonDown("Gun2") && !timeReload)
        {
            if (indexGun != 1)
            {
                indexGun = 1;
                lastChangeTime = 0;
                if (actualGun != null)
                {
                    actualGun.gameObject.SetActive(false);
                    actualGun = null;
                }
                isChangin = true;
            }
        }

        if (Input.GetButtonDown("Gun3") && !timeReload)
        {
            if (indexGun != 2)
            {
                indexGun = 2;
                lastChangeTime = 0;
                if (actualGun != null)
                {
                    actualGun.gameObject.SetActive(false);
                    actualGun = null;
                }
                isChangin = true;
            }
        }

        if(isChangin)
        {
            lastChangeTime += Time.deltaTime;

            if(lastChangeTime >= changeTimeM)
            {
                isChangin = false;
                ChangeGun(indexGun);
                AudioSource.PlayClipAtPoint(changeGun, gameObject.transform.position);
            }
        }
    }

    #endregion

    #region Custom Funtions
    private void Shoot()
    {
        if(Physics.Raycast(player.cam.transform.position, player.cam.transform.forward, out RaycastHit hit,actualGun.data.range))
        {
            if (hit.transform != null)
            {
                Debug.Log($"hit: {hit.transform.name}");
                Instantiate(prefBullHole, hit.point + hit.normal * 0.01f , Quaternion.LookRotation(hit.normal,Vector3.up));
                GameObject go = Instantiate(prefBullHole, hit.point + hit.normal * 0.01f, Quaternion.LookRotation(hit.normal, Vector3.up));
                Destroy(go, 1.5f);

            }
           
        }
        actualGun.data.actualAmmo--;
        lastShootTime = actualGun.data.fireRate;
        AudioSource.PlayClipAtPoint(actualGun.data.disparo, gameObject.transform.position);
        AddRecoil();
        GameObject efect = VisualEffect.Instantiate(vfx, actualGun.muzzlePoint.position, actualGun.muzzlePoint.rotation).gameObject;
        efect.transform.parent = actualGun.muzzlePoint;
        Destroy(efect, 1.5f);
    }

    private void AddRecoil()
    {
        targetRotation += new Vector3(-actualGun.data.recoil.x, Random.Range(-actualGun.data.recoil.y, actualGun.data.recoil.y), 0);
    }

    private void Reload_Time()
    {
        actualGun.data.actualAmmo = actualGun.data.maxAmmoCount;
       
    }

    void ChangeGun(int index)
    {
        if(guns[index] != null)
        {
            actualGun = guns[index];
            actualGun.gameObject.SetActive(true);
        }
    }

    void Interaccion(Transform floor_gun,int index)
    {
        if(actualGun != null)
        {
            ReleasedGun(actualGun, indexGun);
        }
        Rigidbody rb = floor_gun.GetComponent<Rigidbody>();
        rb.isKinematic = true;
        floor_gun.transform.parent = player.gunPoint;
        actualGun = floor_gun.transform.gameObject.GetComponent<Gun>();
        floor_gun.transform.localPosition = actualGun.data.offset;
        floor_gun.transform.localRotation = Quaternion.identity;
        guns[index] = actualGun;
    }

    void ReleasedGun(Gun floor_Gun, int index)
    {
        Rigidbody rb = floor_Gun.GetComponent<Rigidbody>();
        actualGun.transform.parent = null;
        guns[index] = null;
        actualGun = null;
        rb.isKinematic = false;
        rb.AddForce(floor_Gun.transform.up * 2, ForceMode.Impulse);
        rb.AddTorque(floor_Gun.transform.right * 2, ForceMode.Impulse);
    }

    #endregion

}
