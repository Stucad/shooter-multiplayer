using UnityEngine;

public class Player_Controller : MonoBehaviour
{
    #region Variables
    [SerializeField] internal Camera cam;
    [SerializeField] private CharacterController controller;
    [SerializeField] private Transform pov;
    [SerializeField] internal Transform gunPoint;
    [SerializeField] private float walkSpeed = 5;
    [SerializeField] private float runSpeed = 10;
    [SerializeField] private float jumpForce = 12;
    [SerializeField] private float gravityMod = 2.5f;

    private float actualSpeed;
    public Transform recoil;
    private Vector2 mouseInput;
    private Vector3 direccion;
    private Vector3 movement;
    private float horizontalRotationStore;
    private float verticalRotationStore;

    [Header("Ground Detection")]
    [SerializeField] private bool isGround;
    [SerializeField] private float radio;
    [SerializeField] private float distacia;
    [SerializeField] private Vector3 offset;
    [SerializeField] private LayerMask lm;

    
    #endregion

    #region Unity Funiones
    private void Start()
    {
        Cursor.lockState = CursorLockMode.Locked;
        cam = Camera.main;
    }
    private void OnDrawGizmos()
    {
        Gizmos.DrawWireSphere(transform.position + offset, radio);
        if (Physics.SphereCast(transform.position + offset, radio, Vector3.down, out RaycastHit hit, distacia, lm))
        {
            Gizmos.color = Color.green;
            Vector3 endPooint = ((transform.position + offset) + (Vector3.down * distacia));
            Gizmos.DrawWireSphere(endPooint, radio);
            Gizmos.DrawLine(transform.position + offset,endPooint);

            Gizmos.DrawSphere(hit.point, 0.1f);
        }
        else
        {
            Gizmos.color = Color.red;
            Vector3 endPooint = ((transform.position + offset) + (Vector3.down * distacia));
            Gizmos.DrawWireSphere(endPooint, radio);
            Gizmos.DrawLine(transform.position + offset, endPooint);
        }
    }
    private void Update()
    {
        Rotation();
        Move();
    }

    private void LateUpdate()
    {
        cam.transform.position = recoil.position;
        cam.transform.rotation = recoil.rotation;

        gunPoint.transform.position = recoil.position;
        gunPoint.transform.rotation = recoil.rotation;
    }

    #endregion

    #region Custom Functions
    private void Rotation()
    {
        mouseInput = new Vector2(Input.GetAxisRaw("Mouse X"), Input.GetAxisRaw("Mouse Y"));
        horizontalRotationStore += mouseInput.x;
        verticalRotationStore -= mouseInput.y;

        verticalRotationStore = Mathf.Clamp(verticalRotationStore, -60, 60);
     
        transform.rotation = Quaternion.Euler(0,horizontalRotationStore,0);
        pov.transform.localRotation = Quaternion.Euler(verticalRotationStore, 0,0);
    }

    private void Move()
    {
        direccion = new Vector3(Input.GetAxisRaw("Horizontal"), 0, Input.GetAxisRaw("Vertical"));
        float velY = movement.y;
        movement = ((transform.forward * direccion.z) + (transform.right * direccion.x)).normalized;
        movement.y = velY;

        if(Input.GetButton("Fire3"))
        {
            actualSpeed = runSpeed;
        }
        else
        {
            actualSpeed = walkSpeed;
        }

        if(IsGrounded())
        {
            movement.y = 0;
        }
        
        if(Input.GetButtonDown("Jump") && IsGrounded())
        {
            movement.y = jumpForce * Time.deltaTime;
        }
        movement.y += Physics.gravity.y * Time.deltaTime * gravityMod;
        controller.Move(movement *(actualSpeed * Time.deltaTime));

    }

    private bool IsGrounded()
    {
        isGround = false;
        if(Physics.SphereCast(transform.position + offset, radio , Vector3.down , out RaycastHit hit, distacia,lm))
        {
            isGround = true;
        }

        return isGround;
    }

   
    #endregion


}
