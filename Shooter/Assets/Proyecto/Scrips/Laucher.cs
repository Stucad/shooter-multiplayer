using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;
using TMPro;
using Photon.Realtime;

public class Laucher : MonoBehaviourPunCallbacks
{
    #region Variantes
    public static Laucher Instance;
    [SerializeField] private GameObject[] screenObjects;
    [SerializeField] private TMP_Text text;
    [SerializeField] private TMP_InputField roomNameInput;
    [SerializeField] private TMP_Text nameRoom;
    [SerializeField] private TMP_Text error;
    [SerializeField] private GameObject buttonPref;
    [SerializeField] private Transform scrool;
    [SerializeField] private List<RoomButton> buttons = new List<RoomButton>();
    #endregion

    #region Funtions Unity
    private void Awake()
    {
        if (Instance == null)
        {
            Instance = this;
        }
        else
        {
            Destroy(this);
        }

    }


    private void Start()
    {
        text.text = "Connecting to Network...";
        SetScreenOjects(0);
        PhotonNetwork.ConnectUsingSettings();
    }



    #endregion

    #region Photon
    public override void OnConnectedToMaster()
    {
        PhotonNetwork.JoinLobby();
        SetScreenOjects(1);
    }

    public override void OnJoinedRoom()
    {
        nameRoom.text = PhotonNetwork.CurrentRoom.Name;
        SetScreenOjects(3);
    }

    public override void OnCreateRoomFailed(short returnCode, string message)
    {
        error.text = "Ya Existe Un Room Con Ese Nombre";
        SetScreenOjects(4);
    }

    public override void OnRoomListUpdate(List<RoomInfo> roomList)
    {
        for (int i = 0; i < roomList.Count; i++)
        {
            if (roomList[i].RemovedFromList)
            {
                Debug.Log($"Room Name: {roomList[i].Name}");
                for (int j = 0; j < buttons.Count; j++)
                {
                    if (roomList[i].Name == buttons[j].roomInfo.Name)
                    {
                        GameObject go = buttons[j].gameObject;
                        buttons.Remove(buttons[j]);
                        Destroy(go);

                    }
                }
            }
        }

        for (int i = 0; i < roomList.Count; i++)
        {
            if (roomList[i].PlayerCount != roomList[i].MaxPlayers && !roomList[i].RemovedFromList)
            {
                RoomButton newRoomButton = Instantiate(buttonPref, scrool).GetComponent<RoomButton>();
                newRoomButton.SetButtonDetails(roomList[i]);
                buttons.Add(newRoomButton);
            }
        }
    }

    public void JoinRoom(RoomInfo info)
    {
        PhotonNetwork.JoinRoom(info.Name);
        text.text = "joining Room....";
        SetScreenOjects(0);
    }

    #endregion

    #region Custom Funcions

    public void SetScreenOjects(int index)
    {
        for (int i = 0; i < screenObjects.Length; i++)
        {
            screenObjects[i].SetActive(i == index);
        }
    }

    public void CreateRoom()
    {
        if(!string.IsNullOrEmpty(roomNameInput.text))
        {
            RoomOptions options = new RoomOptions();
            options.MaxPlayers = 8;
            text.text = "Create Room....";
            PhotonNetwork.CreateRoom(roomNameInput.text);
            SetScreenOjects(0);
        }
    }


    public void Return()
    {
        PhotonNetwork.LeaveRoom();
        SetScreenOjects(1);
    }
    #endregion

}
