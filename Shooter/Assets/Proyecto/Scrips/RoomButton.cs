using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;
using TMPro;
using Photon.Realtime;
public class RoomButton : MonoBehaviour
{
    [SerializeField] private TMP_Text buttonText;
    [SerializeField] public RoomInfo roomInfo;

    #region Funtions Customs
    public void SetButtonDetails(RoomInfo inputInfo)
    {
        roomInfo = inputInfo; 
        buttonText.text = roomInfo.Name;
    }

    public void joinRoom()
    {
        Laucher.Instance.JoinRoom(roomInfo);
    }
    #endregion
}
