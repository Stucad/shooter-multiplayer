using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Gun : MonoBehaviour
{
    #region Variables
    public Gun_Data data;
    public Transform muzzlePoint;
    #endregion

    #region Unity Funcions
    private void Awake()
    {
        data.actualAmmo = data.maxAmmoCount;
    }
    #endregion
}
